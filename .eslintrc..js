module.exports = {
  "parser": "babel-eslint",
  "extends": [
    "eslint:recommended", 
    "plugin:react/recommended"
  ],
  "env": {
    "browser": true,
    "node": true,
    "es6": true,
  },
  "plugins": [
    "import",
    "promise",
    "compat",
    "react"
  ],
  "parserOptions": {
    "ecmaVersion": 6,
    "sourceType": "module",
    "ecmaFeatures": {
      "jsx": true
    }
  },
  "rules": {
    "react/jsx-filename-extension": ["error", { "extensions": [".js", ".jsx"] }],
    "react/forbid-prop-types": "off",
    "arrow-parens": ["off"],
    "no-confusing-arrow": "off",
  }
};
