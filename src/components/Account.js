import React from 'react';
import PropTypes from 'prop-types';

import withAuthorization from './Session/withAuthorization';
// import { PasswordForgetForm } from './PasswordForget';
// import PasswordChangeForm from './PasswordChange';
import ProfileEdit from './ProfileEdit';

const AccountPage = ({ authUser }) => {
  return (
    <div>
      <ProfileEdit />
      {/*
        <PasswordForgetForm />
        <PasswordChangeForm />
      */}
    </div>
  )
}
AccountPage.contextTypes = {
  authUser: PropTypes.object,
};

const authCondition = (authUser) => !!authUser;

export default withAuthorization(authCondition)(AccountPage);
