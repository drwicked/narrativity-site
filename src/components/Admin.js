import React, { Component } from 'react';
import Flatpickr from 'react-flatpickr';
import { Flex, Box, Input, Label, Textarea } from 'rebass';
import styled from 'styled-components';
import moment from 'moment';
import { FirestoreCollection } from "react-firestore";

import withAuthorization from './Session/withAuthorization';
import { base } from '../firebase/firebase';
import 'flatpickr/dist/themes/material_blue.css'
import { FormItem } from './shared';
import * as Site from '../constants/site'; 
import * as routes from '../constants/routes';

const AdminWrap = styled.div`
  h2 {
    font-size: 1.5em;
    -webkit-margin-before: 0;
    -webkit-margin-after: 0;
    -webkit-margin-start: 0px;
    -webkit-margin-end: 0px;
    font-weight: bold;
    text-align: center;
    width: 100%;
    background-color: ${p => p.theme.blue};
    color: ${p => p.theme.white};
    height: 40px;
  }
  .top {
    border: 2px solid ${p => p.theme.blue};
  }
  overflow: hidden;
  label {
    font-size: 14px;
  }
`;

const PickerWrap = styled.div`
  input {
    font-size: 20px;
    text-align: center;
    width: 300px;
  }
  border-left: 2px solid ${p => p.theme.blue};
  
  overflow: hidden;
  .calWrap {
    margin-top: 10px;
    margin-bottom: 10px;
    text-align: center;
    .flatpickr-calendar {
      margin: 0 auto;
    }
  }
`;
const PanelInputs = styled.div`
  > div {
    
  }
  > button {
    width: 100%;
    margin: 4px 0;
    border-radius: 0;
  }
  textarea {
    min-height: 200px;
  }
`;

const AllUsers = styled.div`
  width: 100%;
  img {
    width: 45px;
    height: 45px;
  }
  ul {
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
  }
  li {
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    padding: 8px;
    &:nth-child(odd) {
      background-color: ${p => p.theme.lightblue};
    }
  }
  .name {
    padding-left: 8px;
    padding-right: 8px;
  }
`;


const INITIAL_STATE = {
  year: 2018,
  theme: '',
  startDate: moment().add(10, 'days').toDate(),
  endDate: moment().add(14, 'days').toDate(),
}


export const adminFields = [
  ['theme', 'Theme', `This year's theme`],
  ['announcement', 'Announcement', `Announcement for the top of the home page`],
  ['intro', 'Intro Text', `Text for the home page, markdown formatted`, 'textarea'],
]

class Admin extends Component {
  constructor(props) {
    super(props);
    this.state = INITIAL_STATE;
  }
  componentWillMount() {
    this.ref = base.syncDoc(`conventions/${Site.conventionId}`, {
        context: this,
        state: 'conInfo',
        then() {
          this.setState({
            loading: false
          })
        },
        onFailure(err) {
          console.log('syncdoc err', err)
          //handle error
        }
      });
  }
  componentWillUnmount(){
    base.removeBinding(this.ref);
  }
  componentDidMount() {
    const { userData } = this.props;
    if (!userData.admin) {
      this.props.history.push(routes.LANDING);
    }
  }
  inputChange = (field, value) => {
    const { conInfo } = this.state;
    conInfo[field] = value;
    this.setState({ conInfo });
  }
  handleDate = ([startDate, endDate]) => this.setState({startDate, endDate})
  render() {
    const { conInfo } = this.state;
  
    if (!conInfo) return <div>loading</div>;
    const { startDate, endDate } = conInfo;
    return(
      <AdminWrap>
        <Flex className="top">
          <Box w={[1, 1/2, 3/5, 3/5]}>
            <h2>Convention Details</h2>
            <PanelInputs>
              {
                adminFields.map(field => (
                  <FormItem key={field[0]}>
                    { field[3] ? (
                        <Textarea
                          id={field[0]}
                          placeholder={field[2]}
                          value={conInfo[field[0]]}
                          onChange={({ target: { value } }) => this.inputChange(field[0], value)}
                        />
                      ) : (
                        <Input
                          id={field[0]}
                          placeholder={field[2]}
                          type="text"
                          value={conInfo[field[0]]}
                          onChange={({ target: { value } }) => this.inputChange(field[0], value)}
                        />
                      )

                    }
                    <Label htmlFor={field[0]}>{field[1]}</Label>
                  </FormItem>
                ))
              }
            </PanelInputs>
          </Box>
          <Box w={[1, 1/2, 2/5, 2/5]}>
            <PickerWrap>
              <h2>Convention Dates</h2>
              <div className="calWrap">
                <Flatpickr
                  value={[startDate, endDate]}
                  options={{
                    inline: true,
                    mode: 'range',
                    minDate: 'today',
                  }}
                  onChange={date => this.handleDate(date)}
                />
              </div>
            </PickerWrap>
          </Box>
        </Flex>
        <Flex flexDirection="column">
          <Box>
            <FirestoreCollection
              path="users"
              render={
                ({ isLoading, data }) => {
                  if (isLoading) return <div>loading</div>;
                  return (
                    <AllUsers>
                      <h2>All Users</h2>
                      <ul>
                        {
                          data.map(({id, username, photoURL}) => {
                            return (
                              <li key={id}>
                                <div>
                                  <img src={photoURL} alt="profile" />
                                </div>
                                <div className="name">{ username }</div>
                              </li>
                            );
                          })
                        }
                      </ul>
                    </AllUsers>
                  );
                }
              }
            />
          </Box>
        </Flex>
      </AdminWrap>
    )
  }
}

// const UserList = ({ users }) =>
//   <div>
//     <h2>List of Users</h2>
//     {Object.keys(users).map(key =>
//       <div key={key}>{users[key].username}</div>
//     )}
//   </div>

const authCondition = (authUser) => !!authUser;

export default withAuthorization(authCondition)(Admin);
