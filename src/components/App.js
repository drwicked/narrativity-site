import React from 'react';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
import { Provider, Container } from 'rebass';
import { FirestoreProvider } from "react-firestore";

import Navigation from './Navigation';
import SchedulePage from './Schedule';
import SignUpPage from './SignUp';
import SignInPage from './SignIn';
import PasswordForgetPage from './PasswordForget';
import HomePage from './Home';
import AccountPage from './Account';
import AdminPage from './Admin';
import PanelViewPage from './PanelView';
import PanelEditPage from './PanelEdit';
import ChatPage from './Chat';
import withAuthentication from './Session/withAuthentication';
import * as routes from '../constants/routes';
import { fbase } from '../firebase/firebase';

const theme = {
  white: '#f8f8ff',
  bg: '#f8f8ff',
  menuBarBg: '#205A91',
  blue: '#63AFC2',
  darkblue: '#205A91',
  aqua: '#559ABA',
  green: '#63BA5C',
  red: '#B14C34',
  lightblue: 'rgba(108, 169, 232, 0.3)',
  lightgreen: 'rgba(108, 232, 169, 0.3)',
}

const AppContainer = Container.extend`
  padding: 0;
  margin: 0;
  font-family: 'Montserrat', sans-serif;
  width: 100%;
  max-width: 100%;
`;

const App = () =>
  <FirestoreProvider firebase={fbase}>
    <Provider theme={theme}>
      <Router>
        <AppContainer fontFamily={'Montserrat'} className="app">
          <Navigation />
          <Container>
            <Route exact path={routes.LANDING} component={HomePage} />
            <Route exact path={routes.HOME} component={HomePage} />
            <Route exact path={routes.SCHEDULE} component={SchedulePage} />
            <Route exact path={routes.SIGN_UP} component={SignUpPage} />
            <Route exact path={routes.SIGN_IN} component={SignInPage} />
            <Route exact path={routes.PASSWORD_FORGET} component={PasswordForgetPage} />
            <Route exact path={routes.ADMIN} component={AdminPage} />
            <Route exact path={routes.ACCOUNT} component={AccountPage} />
            <Route exact path={routes.CHAT} component={ChatPage} />
            <Route path="/panel/:panelId" component={() => <PanelViewPage />} />
            <Route path="/edit/:panelId" component={() => <PanelEditPage />} />
            <Route path="/e/:panelId" component={() => <PanelEditPage view />} />
          </Container>
        </AppContainer>
      </Router>
    </Provider>
  </FirestoreProvider>

export default withAuthentication(App);
