import React, { Component } from 'react';

import { Flex, Box, Input, Button } from 'rebass';
import styled from 'styled-components';
import { FirestoreCollection } from "react-firestore";

import withAuthorization from './Session/withAuthorization';
import { base } from '../firebase/firebase';
import 'flatpickr/dist/themes/material_blue.css'
import { PanelistList } from './shared';

// const Chance = require('chance');
// const chance = new Chance();

export const panelFields = [
  ['title', 'Title', 'Enter the panel title'],
  ['url', 'Short ID', 'url-friendly-panel-id'],
  ['room', 'Room Name', 'Room name'],
  ['notes', 'Notes', 'Notes on this panel'],
  ['description', 'Description', 'Panel Description', 'textarea'],
]

const ChatWrap = Flex.extend`
  h2 {
    font-size: 1.5em;
    -webkit-margin-before: 0;
    -webkit-margin-after: 0;
    -webkit-margin-start: 0px;
    -webkit-margin-end: 0px;
    font-weight: bold;
    text-align: center;
    width: 100%;
    background-color: ${p => p.theme.blue};
    color: ${p => p.theme.white};
    height: 40px;
  }
  border: 2px solid ${p => p.theme.blue};
  overflow: hidden;
  .users {
    background-color: ${p => p.theme.lightgreen};
    height: 100%;
  }
  .chatLog {
    min-height: 200px;
  }
`;
const ChatBox = styled.div`
  display: flex;
  flex-direction: row;
`;

function removeDuplicates(originalArray, prop) {
  var newArray = [];
  var lookupObject  = {};

  for(var i in originalArray) {
    lookupObject[originalArray[i][prop]] = originalArray[i];
  }

  for(i in lookupObject) {
     newArray.push(lookupObject[i]);
  }
  return newArray;
 }


class NewPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chatText: ''
    };
  }
  inputChange = (field, value) => {
    if (field === 'title') {
      this.setState({
        title: value,
        url: value.replace(/\W/g, '-'),
      });
    } else {
      this.setState({
        [field]: value,
      });
    }
    this.checkFields();
  }
  sendChat = () => {
    const { chatText } = this.state;
    const { userData } = this.props;
    const dd = new Date();
    const sent = dd.getTime();
    console.log('sent', sent)
    base.addToCollection('chat', {
      user: userData,
      message: chatText,
      sent,
    }, sent.toString()).then(() => {
      this.setState({ chatText: '' });
    }).catch(err => console.log(err));
  }
  catchEnter = (e) => {
    if (e.key === 'Enter') {
      this.sendChat();
    }
  }
  render() {
    const { chatText } = this.state;
    return(
      <ChatWrap flexDirection="column">
        <FirestoreCollection
          path="chat"
          sort="sent:desc"
          render={({ isLoading, data }) => {
            if (isLoading) return <div>Loading</div>
            const recentUsers = removeDuplicates(
              data.map(chats => chats.user),
              'username'
            );
            return (
              <Flex>
                <Box w={[1, 1/5]}>
                  <h2>Users</h2>
                  <PanelistList className="users">
                    {recentUsers.map(({ id, username }, i) => (
                      <li key={`${id}_${i}`}>
                        {username}
                      </li>
                    ))}
                  </PanelistList>
                </Box>
                <Box w={[1, 4/5]}>
                  <div>
                    <h2>Chat</h2>
                    <div className="chatLog">
                      <ul>
                        {data.map(({ id, user, message }) => (
                          <li key={id}>
                            {user.username} - {message}
                          </li>
                        ))}
                      </ul>
                    </div>
                    <ChatBox>
                      <Input
                        value={chatText}
                        onKeyDown={this.catchEnter}
                        onChange={({ target: { value } }) => this.setState({ chatText: value })}
                      />
                      <Button disabled={chatText===''} onClick={this.sendChat}>Send</Button>
                    </ChatBox>
                  </div>
                </Box>
              </Flex>
            );
          }}
        />
      </ChatWrap>
    )
  }
}

const authCondition = (authUser) => !!authUser;

export default withAuthorization(authCondition)(NewPanel);
