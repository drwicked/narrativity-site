import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { Container } from 'rebass';
import { FirestoreDocument } from "react-firestore";

import ListPanels from './ListPanels';
import * as Site from '../constants/site';

const HomeContainer = Container.extend`
  p.announcement {
    padding: 4px;
    background-color: papayawhip;
  }
`;

class Home extends Component {
  render() {
    console.log('landing');
    return (
      <HomeContainer>
        <FirestoreDocument 
          path={`conventions/${Site.conventionId}`}
          render={({ isLoading, data }) => {
            return isLoading ? (
              <div>Loading</div>
            ) : (
              <div>
                <h1>{Site.title}</h1>
                <h2>{data.theme}</h2>
                <p>{data.announcement}</p>
                <p>{data.intro}</p>
              </div>
            );
          }}
        />
        <ListPanels />
      </HomeContainer>
    );
  }
}


export default Home;

