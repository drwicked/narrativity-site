import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from 'rebass';
import styled from 'styled-components';
import moment from 'moment';
import { FirestoreCollection } from 'react-firestore';
import { Link } from 'react-router-dom';

import withAuthorization from './Session/withAuthorization';
// import * as Site from '../constants/site';
import { db } from '../firebase';
// import { FormItem } from './shared';
const ScheduleWrap = styled.div`
  ul, h2 {
    -webkit-margin-before: 0;
    -webkit-margin-after: 0;
    -webkit-margin-start: 0px;
    -webkit-margin-end: 0px;
    -webkit-padding-start: 0px;
  }
  h2 {
    background-color: ${p => p.theme.blue};
    color: ${p => p.theme.white};
    padding: 4px;
  }
  ul {
    list-style: none;
    li {
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      align-items: center;
      padding: 4px 6px;
      min-height: 60px;
      &:nth-child(odd) {
        background-color: ${p => p.theme.lightgreen};
      }
      &:nth-child(even) {
        background-color: ${p => p.theme.lightblue};
      }
      small {
        font-size: 9px;
        color: #555;
      }
      .time {
        text-align: right;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        .mmdd {
          font-size: smaller;
        }
        .hmm {
        }
      }
      > div {
        width: 100%;
        justify-content: space-between;
      }
    }
  }
`;

class ListPanels extends Component {
  static propTypes = {
    userData: PropTypes.object,
    authUser: PropTypes.object,
  };
  constructor(props) {
    super(props);
    console.log('props', props)
  }
  deleteSpookmark = id => db.deleteSpookmark(id).then(() => console.log('deleted', id))

  renderCollection = ({ isLoading, data }) => {
    if (isLoading) return <div>loading</div>;
    return (
      <ScheduleWrap>
        <h2>Schedule</h2>
        <ul>
          {
            data.map(({id, title, description, url, date, location }) => {
              return (
                <li key={id}>
                  <Flex>
                    <Box className="details" w={[1, 2/3, 4/5, 4/5]}>
                      <Link to={`/panel/${url}`} href={`/panel/${url}`} title={title} alt={title}>{title}</Link><br />
                      <small>{description}</small>
                    </Box>
                    <Box className="time" w={[1, 1/3, 1/5, 1/5]}>
                      <span className="hmm">{ moment(date).format('ddd h:mma') }</span>
                      <span className="mmdd">{ moment(date).format('MM/DD') }</span>
                    </Box>
                  </Flex>
                </li>
              );
            })
          }
        </ul>
      </ScheduleWrap>
    );
  }
  render() {
    return (
      <FirestoreCollection
        path="panels"
        render={this.renderCollection}
      />
    );
  }
}

const authCondition = (authUser) => !!authUser;

export default withAuthorization(authCondition)(ListPanels);

