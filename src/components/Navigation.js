import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Toolbar } from 'rebass';
// import styled from 'styled-components';

import SignOutButton from './SignOut';
import * as routes from '../constants/routes';
import { title } from '../constants/site';
import { Consumer } from './Session/withAuthentication'

import NarrativityIcon from './NarrativityIcon';

const MenuBar = Toolbar.extend`
  background-color: ${p => p.theme.menuBarBg};
  img {
    width: 48px;
    height: 48px;
  }
  .hero {
    font-size: 26px;
  }
  a, button {
    background-color: transparent;
    font-size: 14px;
    font-weight: 700;
    border: 0;
    padding: 8px;
    display: inline-flex;
    align-items: center;
    align-self: stretch;
    text-decoration: none;
    white-space: nowrap;
    cursor: pointer;
    color: #f8f8ff;
  }
  a.spacer {
    margin-left: auto;
  }
`;

const Navigation = (props) =>
  <div>
    { props.authUser ? <NavigationAuth /> : <NavigationNonAuth /> }
  </div>

Navigation.contextTypes = {
  authUser: PropTypes.object,
};

const SiteIcon = () =>
  <div className="icon">
    <NarrativityIcon />
  </div>

const NavigationAuth = () =>
  <MenuBar>
    <SiteIcon />
    <Link className="hero" to={routes.LANDING}>{title}</Link>
    <Link to={routes.HOME}>Home</Link>
    <Link to={routes.SCHEDULE}>Schedule</Link>
    <Link to={routes.CHAT}>Chat</Link>
    <Link className="spacer" to={routes.ACCOUNT}>Account</Link>
    <Link to={routes.ADMIN}>Admin</Link>
    <SignOutButton />
  </MenuBar>

const NavigationNonAuth = () =>
  <MenuBar>
    <SiteIcon />
    <Link className="hero" to={routes.LANDING} href={routes.LANDING}>{title}</Link>
    <Link className="spacer" to={routes.SIGN_UP} href={routes.SIGN_UP}>Sign Up</Link>
    <Link to={routes.SIGN_IN} href={routes.SIGN_IN}>Sign In</Link>
  </MenuBar>

const NavigationWrap = props => (
  <Consumer>
    {session => <Navigation {...props} {...session} />}
  </Consumer>
); 

export default NavigationWrap;
