import React, { Component } from 'react';
import Flatpickr from 'react-flatpickr';
import { Flex, Box, Input, Label, Button, Textarea } from 'rebass';
import styled from 'styled-components';
import moment from 'moment';
import { FirestoreCollection } from "react-firestore";

import { firestoreDb } from '../firebase/firebase';
import withAuthorization from './Session/withAuthorization';
import { db } from '../firebase';
import 'flatpickr/dist/themes/material_blue.css'
import { FormItem, enumerateDates, PanelistList } from './shared';
import * as Site from '../constants/site';
import FindPeople from './FindPeople';
import { base } from '../firebase/firebase';

export const panelFields = [
  ['title', 'Title', 'Enter the panel title'],
  ['url', 'Short ID', 'url-friendly-panel-id'],
  ['room', 'Room Name', 'Room name'],
  ['notes', 'Notes', 'Notes on this panel'],
  ['description', 'Description', 'Panel Description', 'textarea'],
]

const PanelEditWrap = Flex.extend`
  h2 {
    font-size: 1.5em;
    -webkit-margin-before: 0;
    -webkit-margin-after: 0;
    -webkit-margin-start: 0px;
    -webkit-margin-end: 0px;
    font-weight: bold;
    text-align: center;
    width: 100%;
    background-color: ${p => p.theme.blue};
    color: ${p => p.theme.white};
    height: 40px;
  }
  border: 2px solid ${p => p.theme.blue};
  overflow: hidden;
  .panelists {
    width: 100%;
    input {
      width: 100%;
      font-size: 16px;
    }
  }
`;

const PickerWrap = styled.div`
  input {
    font-size: 20px;
    text-align: center;
    width: 300px;
  }
  
  border-bottom: 0;
  
  overflow: hidden;
  .calWrap {
    margin-top: 10px;
    margin-bottom: 10px;
    text-align: center;
    .flatpickr-calendar {
      margin: 0 auto;
    }
  }
`;
const PanelInputs = styled.div`
  > div {
    
  }
  > button {
    width: 100%;
    margin: 4px 0;
    border-radius: 0;
  }
  textarea {
    min-height: 110px;
  }
  border-right: 2px solid ${p => p.theme.blue};
`;
const DayBtns = styled.div`
  display: flex;
  flex-direction: row;
  button {
  }
`;
const DayBtn = Button.extend`
  background-color: ${p => p.selected ? p.theme.green : p.theme.blue};
  border-radius: 0;
`;

const PeopleList = styled.div`

`;


const INITIAL_STATE = {
  url: '',
  title: '',
  notes: '',
  description: '',
  panelists: [],
}

class PanelEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: moment().add(1, 'days').valueOf(),
      conDates: [],
      dateRange: [],
      dateSelected: '',
      ...INITIAL_STATE,
    }
  }
  componentWillMount() {
    const { match: { params: { panelId } } } = this.props;
    this.ref = base.syncDoc(`panels/${panelId}`, {
      context: this,
      state: 'panelInfo',
      then() {
        this.setState({
          loading: false
        })
      },
      onFailure(err) {
        console.log('syncdoc err', err)
      }
    });
    this.getInfo();
  }
  componentWillUnmount(){
    base.removeBinding(this.ref);
  }
  async getInfo() {
    const data = await db.onceGetInfo(Site.conventionId).then(snap => {
      if (snap.exists) {
        const conData = snap.data();
        const { startDate, endDate } = conData;
        const defaultStart = moment(startDate).add(14, 'hours').toDate();
        const dateRange = enumerateDates([startDate, endDate]);
        this.setState({
          dateSelected: moment(startDate).format('ddd MM/DD'),
          date: defaultStart,
          conDates: [ startDate, endDate ],
          dateRange,
        });
        return conData;
      } else {
        return false
      }
    });
    return data;
  }
  addPanel = () => {
    const { userData } = this.props;
    const { url } = this.state;
    const createdDate = Date.now();
    const newPanelData = {
      createdBy: userData.uid,
      createdDate,
      ...this.state
    };
    db.addPanel(url, newPanelData).then(ref => {
      this.setState({ ...INITIAL_STATE });
    });
  }
  selectDay = dateSelected => {
    const { date, conDates: [startDate] } = this.state;
    // const day = moment(dateSelected, 'ddd MM/DD').format('ddd MM/DD');
    const time = moment(date).format('HH:mm');
    const year = moment(startDate).format('YYYY');
    const newDateStr = `${dateSelected}/${year} ${time}`;
    const newDate = moment(newDateStr, 'ddd MM/DD/YYYY HH:mm').toDate();
    this.setState({
      dateSelected,
      date: newDate,
    });
  }
  inputChange = (field, value) => {
    if (field === 'title') {
      this.setState({
        title: value,
        url: value.replace(/\W/g, '-'),
      });
    } else {
      this.setState({
        [field]: value,
      });
    }
  }
  addPanelist = (event, {suggestion}) => {
    const { panelInfo } = this.state;
    const { panelists = [] } = panelInfo;
    const newPanelistRef = firestoreDb.collection('users').doc(suggestion.uid);
    this.setState({
      panelInfo: {
        ...panelInfo,
        panelists: [
          ...panelists,
          {
            ...suggestion,
            ref: newPanelistRef
          }
        ]
      }
    })
  }
  render() {
    const { panelInfo, dateRange, dateSelected } = this.state;
    if (!panelInfo) return <div>Loading</div>
    const { date, panelists=[] } = panelInfo;
    return(
      <PanelEditWrap>
        <Box w={[1, 1/2, 3/5, 3/5]}>
          <h2>New Panel</h2>
          <PanelInputs>
            {
              panelFields.map(field => (
                <FormItem key={field[0]}>
                  { field[3] ? (
                      <Textarea
                        id={field[0]}
                        placeholder={field[2]}
                        value={panelInfo[field[0]]}
                        onChange={({ target: { value } }) => this.inputChange(field[0], value)}
                      />
                    ) : (
                      <Input
                        id={field[0]}
                        placeholder={field[2]}
                        type="text"
                        value={panelInfo[field[0]]}
                        onChange={({ target: { value } }) => this.inputChange(field[0], value)}
                      />
                    )
                  }
                  <Label htmlFor={field[0]}>{field[1]}</Label>
                </FormItem>
              ))
            }
          </PanelInputs>
        </Box>
        <Box w={[1, 1/2, 2/5, 2/5]}>
          <PickerWrap>
            <h2>Date & Time</h2>
            <DayBtns>
              {
                dateRange.map(dateStr => (
                  <DayBtn
                    selected={dateStr === dateSelected}
                    onClick={() => this.selectDay(dateStr)}
                    key={dateStr}
                  >
                    {dateStr}
                  </DayBtn>
                ))
              }
            </DayBtns>
            <div className="calWrap">
              <Flatpickr
                value={date}
                options={{
                  inline: true,
                  minDate: 'today',
                  minuteIncrement: 15,
                  enableTime: true,
                  dateFormat: 'D h:i K',
                  noCalendar: true,
                }}
                onChange={date => { this.setState({date}) }}
              />
            </div>
          </PickerWrap>
          <div className="panelists">
            <FirestoreCollection
              path="users"
              render={
                ({ isLoading, data }) => {
                  if (isLoading) return <div>loading</div>;
                  return (
                    <PeopleList>
                      <PanelistList>
                        {
                          panelists.map(({id, username, photoURL}) => {
                            return (
                              <li key={id}>
                                <div>
                                  <img alt={username} src={photoURL} />
                                </div>
                                <div className="name">{ username }</div>
                              </li>
                            );
                          })
                        }
                      </PanelistList>
                      <FindPeople data={data} addPanelist={this.addPanelist} />
                    </PeopleList>
                  );
                }
              }
            />
          </div>
        </Box>
      </PanelEditWrap>
    )
  }
}

const authCondition = (authUser) => !!authUser;

export default withAuthorization(authCondition)(PanelEdit);
