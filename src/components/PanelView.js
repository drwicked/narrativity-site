import React, { Component } from 'react';
import { Flex, Box, Textarea } from 'rebass';
import styled from 'styled-components';
import moment from 'moment';
import { FirestoreDocument } from "react-firestore";

import withAuthorization from './Session/withAuthorization';
import { PanelistList } from './shared';

const Wrap = styled.div`
  h2 {
    font-size: 1.5em;
    -webkit-margin-before: 0;
    -webkit-margin-after: 0;
    -webkit-margin-start: 0px;
    -webkit-margin-end: 0px;
    font-weight: bold;
    text-align: center;
    width: 100%;
    background-color: ${p => p.theme.blue};
    color: ${p => p.theme.white};
    height: 40px;
  }
  .top {
    border: 2px solid ${p => p.theme.blue};
  }
  .rightSide {
    
  }
  overflow: hidden;
  label {
    font-size: 14px;
  }
`;

const INITIAL_STATE = {
  year: 2018,
  theme: '',
  startDate: moment().add(10, 'days').toDate(),
  endDate: moment().add(14, 'days').toDate(),
}


export const profileFields = [
  ['username', 'Name', `Your name`],
  ['email', 'Your email', `your@email.com`],
  ['bio', 'Bio', `Tell people a little more about you.`, 'textarea'],
]

class PanelView extends Component {
  constructor(props) {
    super(props);
    this.state = INITIAL_STATE;
  }
  render() {
    const { match: { params: { panelId } } } = this.props;
    return(
      <FirestoreDocument 
        path={`panels/${panelId}`}
        render={({ isLoading, data }) => {
          if (isLoading) {
            return (<div>Loading</div>)
          }
          const { panelists } = data;
          return (
            <Wrap>
              <Flex className="top">
                <Box w={[1, 1/2, 3/5, 3/5]}>
                  <h2>Panel Info</h2>
                  <h3>{ data.title }</h3>
                  <p>{ moment(data.date).format('ddd HH:mma') }</p>
                  <p>{ data.description }</p>
                </Box>
                <Box className="rightSide" w={[1, 1/2, 2/5, 2/5]}>
                  <h2>Panelists</h2>
                  <PanelistList>
                    { panelists.map(panelist =>
                      <li key={panelist.id}>
                        <div>
                          <img src={panelist.photoURL} alt="profile" />
                        </div>
                        <div className="name">{panelist.username}</div>
                      </li>
                    )}
                  </PanelistList>
                  <h2>Notes</h2>
                  <Textarea />
                </Box>
              </Flex>
            </Wrap>
          );
        }}
      />
    )
  }
}

const authCondition = (authUser) => !!authUser;

export default withAuthorization(authCondition)(PanelView);
