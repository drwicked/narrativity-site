import React, { Component } from 'react';
import { Flex, Box, Input, Label, Textarea } from 'rebass';
import styled from 'styled-components';
import FileUploader from 'react-firebase-file-uploader';
import moment from 'moment';

import withAuthorization from './Session/withAuthorization';
import { base } from '../firebase/firebase';
import 'flatpickr/dist/themes/material_blue.css'
import { FormItem } from './shared';
import { fbase } from '../firebase/firebase';

const Wrap = styled.div`
  h2 {
    font-size: 1.5em;
    -webkit-margin-before: 0;
    -webkit-margin-after: 0;
    -webkit-margin-start: 0px;
    -webkit-margin-end: 0px;
    font-weight: bold;
    text-align: center;
    width: 100%;
    background-color: ${p => p.theme.blue};
    color: ${p => p.theme.white};
    height: 40px;
  }
  .top {
    border: 2px solid ${p => p.theme.blue};
  }
  .rightSide {
    text-align: center;
  }
  overflow: hidden;
  label {
    font-size: 14px;
  }
`;
const PanelInputs = styled.div`
  > div {
    
  }
  > button {
    width: 100%;
    margin: 4px 0;
    border-radius: 0;
  }
  textarea {
    min-height: 200px;
  }
`;
const ImageUploader = styled.div`
  display: flex
  flex-direction: column;
  justify-content: center;
  align-items: center;
  .avatarDisplay {
    background-color: ${p => p.theme.aqua};
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 200px;
    height: 200px;
    border-radius: 50%;
    margin: 10px 0;

    img {
      width: 128px;
      height: 128px;
      border-radius: ${p => p.theme.borderRadius};
    }
  }
`;

const INITIAL_STATE = {
  year: 2018,
  theme: '',
  startDate: moment().add(10, 'days').toDate(),
  endDate: moment().add(14, 'days').toDate(),
}


export const profileFields = [
  ['username', 'Name', `Your name`],
  ['email', 'Your email', `your@email.com`],
  ['bio', 'Bio', `Tell people a little more about you.`, 'textarea'],
]

class ProfileEdit extends Component {
  constructor(props) {
    super(props);
    this.state = INITIAL_STATE;
  }
  componentWillMount() {
    const { authUser } = this.props;
    this.ref = base.syncDoc(`users/${authUser.uid}`, {
        context: this,
        state: 'userInfo',
        then() {
          this.setState({
            loading: false
          })
        },
        onFailure(err) {
          console.log('syncdoc err', err)
        }
      });
  }
  componentWillUnmount(){
    base.removeBinding(this.ref);
  }
  inputChange = (field, value) => {
    const { userInfo } = this.state;
    userInfo[field] = value;
    this.setState({ userInfo });
  }
  handleUploadStart = () => this.setState({isUploading: true, progress: 0});
  handleProgress = (progress) => this.setState({progress});
  handleUploadError = (error) => {
    this.setState({isUploading: false});
    console.error(error);
  }
  handleUploadSuccess = (filename) => {
    this.setState({
      progress: 100,
      isUploading: false
    });
    fbase.storage()
      .ref('images')
      .child(filename)
      .getDownloadURL()
      .then(url => 
        this.setState((state) => ({
          userInfo: {
            ...state.userInfo,
            photoURL: url
          }
        }))
      );
  };
  render() {
    const { userInfo, progress } = this.state;
    console.log('progress', progress)
    if (!userInfo) return <div>loading</div>;
    const { photoURL } = userInfo;
    return(
      <Wrap>
        <Flex className="top">
          <Box w={[1, 1/2, 3/5, 3/5]}>
            <h2>Convention Details</h2>
            <PanelInputs>
              {
                profileFields.map(field => (
                  <FormItem key={field[0]}>
                    { field[3] ? (
                        <Textarea
                          id={field[0]}
                          placeholder={field[2]}
                          value={userInfo[field[0]]}
                          onChange={({ target: { value } }) => this.inputChange(field[0], value)}
                        />
                      ) : (
                        <Input
                          id={field[0]}
                          placeholder={field[2]}
                          type="text"
                          value={userInfo[field[0]]}
                          onChange={({ target: { value } }) => this.inputChange(field[0], value)}
                        />
                      )

                    }
                    <Label htmlFor={field[0]}>{field[1]}</Label>
                  </FormItem>
                ))
              }
            </PanelInputs>
          </Box>
          <Box className="rightSide" w={[1, 1/2, 2/5, 2/5]}>
            <ImageUploader>
              <div className="avatarDisplay">
                {photoURL && (
                  <img alt="avatar" src={photoURL} />
                )}
              </div>
              {this.state.isUploading &&
                <p>Progress: {this.state.progress}</p>
              }
            </ImageUploader>
            <label className="btn">
              Upload Profile Photo
              <FileUploader
                accept="image/*"
                name="avatar"
                hidden
                randomizeFilename
                storageRef={fbase.storage().ref('images')}
                onUploadStart={this.handleUploadStart}
                onUploadError={this.handleUploadError}
                onUploadSuccess={this.handleUploadSuccess}
                onProgress={this.handleProgress}
              />
            </label>
          </Box>
        </Flex>
      </Wrap>
    )
  }
}

const authCondition = (authUser) => !!authUser;

export default withAuthorization(authCondition)(ProfileEdit);
