import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container } from 'rebass';
// import styled from 'styled-components';
// import moment from 'moment';
// import { FirestoreCollection } from "react-firestore";

import withAuthorization from './Session/withAuthorization';
import ListPanels from './ListPanels';
import * as Site from '../constants/site';
// import { FormItem } from './shared';
import PanelCreate from './PanelCreate';

const LandingPage = () =>
  <Container>
    <h1>Narrativity is coming</h1>
    <p>{ Site.about }</p>
    <ul>{ Site.promoPoints.map((pt, i) => <li key={i}>{pt}</li>)}</ul>
  </Container>


class Landing extends Component {
  static propTypes = {
    userData: PropTypes.object,
    authUser: PropTypes.object,
  };
  render() {
    const { authUser, userData } = this.props;
    console.log('userData', userData)
    if (!authUser) return <LandingPage />;
    return (
      <Container>
        <PanelCreate />
        <ListPanels />
      </Container>
    );
  }
}

const authCondition = (authUser) => !!authUser;

export default withAuthorization(authCondition)(Landing);

