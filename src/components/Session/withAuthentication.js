import React from 'react';
// import PropTypes from 'prop-types';
// import { withRouter } from 'react-router-dom';

// import Loading from '../Loading';
import { firebase, db } from '../../firebase';
// import * as routes from '../../constants/routes';

const sessionState = {
  authUser: null,
  userData: null,
  loaded: false,
  userDataLoaded: false,
};
export const { Provider, Consumer } = React.createContext(sessionState);

export const withAuthentication = (Component) => {
  class WithAuthentication extends React.Component {
    constructor(props) {
      super(props);
      this.state = sessionState;
    }
    componentDidMount() {
      firebase.auth.onAuthStateChanged(authUser => {
        if (authUser) {
          const simpleAuthUser = {
            uid: authUser.uid,
            email: authUser.email,
          }
          this.setState({ authUser: simpleAuthUser });
          if (!this.state.userDataLoaded) {
            this.getUserData(authUser.uid).then((userData) => {
              this.setState({ userData, userDataLoaded: true, loaded: true });
            })
          } else {
            console.log(':: already loaded?');
          }
        } else {
          console.log('not yet logged in');
          this.setState(() => ({ authUser: null, loaded: true }));
        }
      });
    }
    async getUserData(authUserUid) {
      const data = await db.onceGetUser(authUserUid).then(dbData => {
        if (dbData.exists) {
          console.log('userData got');
          const userData = dbData.data();
          return userData;
          // db.getUserFriends(authUserUid).then(friends => {
          //   userData.friends = friends;
          //   console.log('friends got');
          //   this.setState({ authUser, userData });
          // });
        } else {
          console.log('bad');
          return false
        }
      });
      return data;
    }
    render() {
      if (!this.state.loaded) return null;
      return (
        <Provider value={this.state}>
          <Component {...this.props} />
        </Provider>
      );
    }
  }

  return WithAuthentication;
};

export default withAuthentication;
