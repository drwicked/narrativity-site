import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import { firebase } from '../../firebase';
import * as routes from '../../constants/routes';
import { Consumer } from './withAuthentication';

const withAuthorization = (condition) => (Component) => {
  class WithAuthorization extends React.Component {
    componentDidMount() {
      firebase.auth.onAuthStateChanged(authUser => {
        console.log('authUser', authUser)
        if (!condition(authUser)) {
          console.log('not authorized', condition(authUser));
          this.props.history.push(routes.LANDING);
        }
      });
    }
    render() {
      return (
        <Consumer>
          {session => <Component {...this.props} {...session} />}
        </Consumer>
      );
    }
  }

  WithAuthorization.propTypes = {
    history: PropTypes.object.isRequired,
  };

  return withRouter(WithAuthorization);
};

export default withAuthorization;
