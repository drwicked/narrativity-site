import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Container, Input, Button, Message, Label, Flex, Box, Card } from 'rebass';

import { SignUpLink } from './SignUp';
import { PasswordForgetLink } from './PasswordForget';
import { auth } from '../firebase';
import * as routes from '../constants/routes';
import { FormItem } from './shared';

const SignInPage = ({ history }) =>
  <Container>
    <Flex flexDirection="column" alignItems="center">
      <Box w={[1, 1/2, 2/3, 1/3]}>
        <Card>
          <h1>Sign In</h1>
          <SignInForm history={history} />
          <PasswordForgetLink />
        </Card>
      </Box>
    </Flex>
  </Container>

const updateByPropertyName = (propertyName, value) => () => ({
  [propertyName]: value,
});

const INITIAL_STATE = {
  email: '',
  password: '',
  error: null,
};

class SignInForm extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  onSubmit = (event) => {
    const {
      email,
      password,
    } = this.state;

    const {
      history,
    } = this.props;

    auth.doSignInWithEmailAndPassword(email, password)
      .then(() => {
        this.setState(() => ({ ...INITIAL_STATE }));
        history.push(routes.HOME);
      })
      .catch(error => {
        this.setState(updateByPropertyName('error', error));
      });

    event.preventDefault();
  }

  render() {
    const {
      email,
      password,
      error,
    } = this.state;

    const isInvalid =
      password === '' ||
      email === '';

    return (
      <form onSubmit={this.onSubmit}>
        <FormItem>
          <Input
            value={email}
            onChange={event => this.setState(updateByPropertyName('email', event.target.value))}
            type="text"
            placeholder="Email Address"
          />
          <Label>Email</Label>
        </FormItem>
        <FormItem>
          <Input
            value={password}
            onChange={event => this.setState(updateByPropertyName('password', event.target.value))}
            type="password"
            placeholder="Password"
          />
          <Label>Password</Label>
        </FormItem>
        <Flex flexDirection="row" justifyContent="space-between">
          <Box>
            <Button disabled={isInvalid} type="submit">
              Sign In
            </Button>
          </Box>
          <Box>
            <SignUpLink />
          </Box>
        </Flex>
        { error && <Message>{error.message}</Message> }
      </form>
    );
  }
}

export default withRouter(SignInPage);

export {
  SignInForm,
};
