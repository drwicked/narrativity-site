import React from 'react';
import { NavLink, Button } from 'rebass';

import { auth } from '../firebase';

const SignOutButton = () =>
  <NavLink>
    <Button
      type="button"
      onClick={auth.doSignOut}
    >
      Sign Out
    </Button>
  </NavLink>

export default SignOutButton;
