import React, { Component } from 'react';
import {
  Link,
  withRouter,
} from 'react-router-dom';
import { Container, Label, Input, Button, Message, Flex, Box } from 'rebass';

import { auth, db } from '../firebase';
import * as routes from '../constants/routes';
import { FormItem } from './shared';

const defaultUserObject = {
  admin: false,
  userType: 'attendee',
  paid: false,
  photoURL: './img/256.png'
}

const Wrap = Container.extend`
  width: 100%;
`;

const SignUpPage = ({ history }) =>
  <Wrap>
    <Flex flexDirection="column" alignItems="center">
      <Box w={[1, 2/3, 2/3, 1/3]}>
        <Wrap>
          <h1>Sign Up</h1>
          <SignUpForm history={history} />
        </Wrap>
      </Box>
    </Flex>
  </Wrap>

const updateByPropertyName = (propertyName, value) => () => ({
  [propertyName]: value,
});

const INITIAL_STATE = {
  username: '',
  email: '',
  passwordOne: '',
  passwordTwo: '',
  error: null,
};

class SignUpForm extends Component {
  constructor(props) {
    super(props);
    this.state = { ...INITIAL_STATE };
  }
  onSubmit = (event) => {
    const {
      username,
      email,
      passwordOne,
    } = this.state;

    const {
      history,
    } = this.props;

    auth.doCreateUserWithEmailAndPassword(email, passwordOne)
      .then(authUser => {
        const joinedDate = Date.now();
        db.doAddUser({
          ...defaultUserObject,
          uid: authUser.uid,
          joinedDate,
          username,
          email,
          photoURL: authUser.photoURL || defaultUserObject.photoURL,
        }).then(() => {
          this.setState(() => ({ ...INITIAL_STATE }));
          history.push(routes.HOME);
        })
        .catch(error => {
          this.setState(updateByPropertyName('error', error));
        });

      })
      .catch(error => {
        this.setState(updateByPropertyName('error', error));
      });

    event.preventDefault();
  }

  render() {
    const {
      username,
      email,
      passwordOne,
      passwordTwo,
      error,
    } = this.state;

    const isInvalid =
      passwordOne !== passwordTwo ||
      passwordOne === '' ||
      username === '' ||
      email === '';

    return (
      <form onSubmit={this.onSubmit}>
        <FormItem>
          <Input
            value={username}
            onChange={event => this.setState(updateByPropertyName('username', event.target.value))}
            type="text"
            placeholder="Name"
          />
          <Label>Name</Label>
        </FormItem>
        <FormItem>
          <Input
            value={email}
            onChange={event => this.setState(updateByPropertyName('email', event.target.value))}
            type="text"
            placeholder="Email Address"
          />
          <Label>Email</Label>
        </FormItem>
        <FormItem>
          <Input
            value={passwordOne}
            onChange={event => this.setState(updateByPropertyName('passwordOne', event.target.value))}
            type="password"
            placeholder="Password"
          />
          <Label>Password</Label>
        </FormItem>
        <FormItem>
          <Input
            value={passwordTwo}
            onChange={event => this.setState(updateByPropertyName('passwordTwo', event.target.value))}
            type="password"
            placeholder="Confirm Password"
          />
          <Label>Confirm</Label>
        </FormItem>
        <FormItem>
          <Button disabled={isInvalid} type="submit">
            Sign Up
          </Button>
        </FormItem>
        { error && <Message>{error.message}</Message> }
      </form>
    );
  }
}

const SignUpLink = () =>
  <div>
    {`Don't have an account? `}
    <Link to={routes.SIGN_UP}><Button>Sign Up</Button></Link>
  </div>

export default withRouter(SignUpPage);

export {
  SignUpForm,
  SignUpLink,
};
