import styled, { keyframes } from 'styled-components';
import moment from 'moment';

const opacityPulse = keyframes`
  0% {opacity: 0.4; transform: rotate(-8deg);}
  25% {opacity: 0.6; transform: rotate(0deg);}
  50% {opacity: 0.4; transform: rotate(8deg);}
  75% {opacity: 0.6; transform: rotate(0deg);}
  100% {opacity: 0.4; transform: rotate(-8deg);}
`;

export const Loading = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background: url('') no-repeat center;
  animation: ${opacityPulse} 2s linear;
  animation-iteration-count: infinite; 
  opacity: 1;
`;

export const FormItem = styled.div`
  display: flex;
  flex-direction: row-reverse;
  margin-bottom: 10px;
  label {
    background-color: ${p => p.theme.blue};
    min-width: 130px;
    font-size: 18px;
    text-align: right;
  }
  label, input {
    padding: 6px;
    height: 40px;
  }
  input {

  }
  input:focus + label {
    background-color: ${p => p.theme.green};
    font-weight: bold;
  }
  button {
    font-size: 30px;
  }
`;

export const PanelistList = styled.ul`
  width: 100%;
  img {
    width: 45px;
    height: 45px;
  }
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  li {
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    padding: 8px;
    &:nth-child(odd) {
      background-color: ${p => p.theme.lightblue};
    }
  }
  .name {
    padding-left: 8px;
    padding-right: 8px;
    font-size: 20px;
  }
`;


export const formatMin = (mins) => {
  if (mins >= 24 * 60 || mins < 0) {
    throw new RangeError("Valid input should be greater than or equal to 0 and less than 1440.");
  }
  const h = mins / 60 | 0;
  const m = mins % 60 | 0;
  if (mins < 60) return `${mins}m`;
  return `${h}h${m}m`;
  // return moment.utc().hours(h).minutes(m).format("h:mm");
};

export function countWords(s){
  s = s.replace(/(^\s*)|(\s*$)/gi,"");//exclude  start and end white-space
  s = s.replace(/[ ]{2,}/gi," ");//2 or more space to 1
  s = s.replace(/\n /,"\n"); // exclude newline with a start spacing
  return s.split(' ').length; 
}

export const wordcountFromProgress = progressArray => {
  const intArray = progressArray.map(pr => pr.wordcount);
  return intArray.reduce((total, num) => total + num)
}

export const enumerateDates = (dateArr = []) => {
  const [startDate, endDate] = dateArr
  let now = moment(startDate).clone();
  const dates = [];
  while (now.isSameOrBefore(moment(endDate))) {
    dates.push(now.format('ddd MM/DD'));
    now.add(1, 'days');
  }
  return dates;
}

