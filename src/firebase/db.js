import { firestoreDb } from './firebase';

export const doCreateUser = (id, username, email) =>
  firestoreDb.ref(`users/${id}`).set({
    username,
    email,
  });

export const onceGetUser = id => firestoreDb.collection('users').doc(id).get();



export const onceGetUsers = () => firestoreDb.collection('users').get();

const defaultUser = {
  karma: 0,
};

export const doAddUser = (userData) => {
  const { uid, username, photoURL, email } = userData;
  console.log('doAddUser', userData, uid, username, email);
  return firestoreDb.collection('users').doc(uid).set({
    ...defaultUser,
    uid,
    username,
    photoURL,
    email,
  });
};

const defaultPanel = {
  started: false,
}

export const deleteSpookmark = id => firestoreDb.collection('panels').doc(id).delete();

export const addPanel = (id, data) => firestoreDb.collection('panels').doc(id).set({
  ...defaultPanel,
  ...data
}).then((docRef) => {
  console.log('created', docRef);
  return docRef;
}).catch(err => console.log(err));

const defaultInfo = {
  startDate: Date.now(),
  endDate: Date.now(),
}

export const onceGetInfo = id => firestoreDb.collection('conventions').doc(id).get();

export const saveInfo = (id, data) => firestoreDb.collection('conventions').doc(id).set({
  ...defaultInfo,
  ...data
}).then((docRef) => {
  console.log('created', docRef);
  return docRef;
}).catch(err => console.log(err));
