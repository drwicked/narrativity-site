import * as firebase from 'firebase';
import Rebase from 're-base';
import '@firebase/firestore';

const config = {
  apiKey: process.env.REACT_APP_APIKEY,
  authDomain: process.env.REACT_APP_AUTHDOMAIN,
  databaseURL: process.env.REACT_APP_DATABASEURL,
  projectId: process.env.REACT_APP_PROJECTID,
  storageBucket: process.env.REACT_APP_STORAGEBUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGINGSENDERID,
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

export const fbase = firebase;
export const provider = new firebase.auth.GoogleAuthProvider();
export const db = firebase.database();
export const auth = firebase.auth();
export const storage = firebase.storage();
export const firestoreDb = firebase.firestore();
export const base = Rebase.createClass(firestoreDb);
